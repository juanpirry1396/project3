package ertzil.com.huella;

import org.junit.Test;

import static org.junit.Assert.*;

public class FingerprintActivityTest {

    @Test
    public void onCreate() {
        String ua = "ua";
        assertEquals(ua,"ua");
    }

    @Test
    public void generateKey() {
        String ua = "ua";
        assertEquals(ua,"ua");
    }

    @Test
    public void cipherInit() {
        String ua = "ua";
        assertEquals(ua,"ua");
    }
}